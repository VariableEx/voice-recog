import speech_recognition as sr


# this use to recognize our audio
r = sr.Recognizer()

with sr.Microphone() as source:
    print('speak anything')
    audio = r.listen(source)

    try:
        text = r.recognize_google(audio)
        print('you said '+str(text))

    except:
        print('we could not recognize you voice')